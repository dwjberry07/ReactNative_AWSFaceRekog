import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { Button, Image, Text, View, Platform } from 'react-native';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
import * as ImageManipulator from 'expo-image-manipulator';
import { Buffer } from 'buffer';

import styles from './styles';

export default class CameraPage extends React.Component {

    image1Base64 = null;
    image2Base64 = null;

    state = {
        image1: null,
        image2: null,
        similarities: '0',
        //hasCameraPermission: null,
    };

    errorPrinter(err) {
      console.log('---------------ERROR---------------');
      console.log(err);
    }

    componentDidMount() {
        this.getPermissionAsync();
    }

    getPermissionAsync = async () => {
        if (Platform.OS !== 'web') {
            const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
            if (status !== 'granted') {
            alert('Sorry, we need camera roll permissions to make this work!');
            }
        }
    };

    render() {
        let { image1, image2 } = this.state;

        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <View style={{ flexDirection: "row" }}>
                <View style={{ flexDirection: "column", padding:20 }}>
                  {image1 && <Image source={{ uri: image1 }} style={{ width: 200, height: 200 }} />}
                  <Button title="Choose Image 1" onPress={this.pickImage1} />
                </View>
                <View style= {{ flexDirection: "column", padding:20 }}>
                  {image2 && <Image source={{ uri: image2 }} style={{ width: 200, height: 200 }} />}
                  <Button title="Choose Image 2" onPress={this.pickImage2} />
                </View>
            </View>
              <View style={{ alignItems: 'center', flexDirection: 'column' }}>
              <Text>
                { this.state.similarities } %
                </Text>
                <Button title="Compare Pictures" onPress={ this.compare }></Button>
              </View>
            </View>
          );
    };

    pickImage1 = async () => {
        try {
          let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            base64: true,
            allowsEditing: true,
            aspect: [4, 3],
            quality: 1,
          });
          if (!result.cancelled) {
            this.image1Base64 = result.base64;
            this.setState({ image1: result.uri });
          }
          console.log(result.uri);
        } catch (err) {
          errorPrinter(err);
        }
    };

    pickImage2 = async () => {
      try {
        let result = await ImagePicker.launchImageLibraryAsync({
          mediaTypes: ImagePicker.MediaTypeOptions.All,
          base64: true,
          allowsEditing: true,
          aspect: [4, 3],
          quality: 1,
        });
        if (!result.cancelled) {
          this.image2Base64 = result.base64;;
          this.setState({ image2: result.uri });
        }
        console.log(result.uri);
      } catch (err) {
        errorPrinter(err);
      }
  };

  compare = async () => {
    
    this.setState({ similarities: 'Calculating '});

    const AWS = require('aws-sdk');
    
    var pSourceBlob, pTargetBlob;

    var photo_source = null;
    var photo_target = null;
      
    if (this.image1Base64 != null && this.image2Base64 != null) {
      photo_source = Buffer.from(this.image1Base64, 'base64');
      photo_target = Buffer.from(this.image2Base64, 'base64');
      console.log(typeof photo_source);
      console.log(typeof photo_target);
    }
    else {
      console.log("\nERROR WITH IMAGES");
      return;
    }
    
    
    var credentials = new AWS.Credentials({
      accessKeyId: 'Access Key'
      secretAccessKey: 'Secret Access Key',
    });
    AWS.config = new AWS.Config({ 
      credentials: credentials,
      region: 'us-east-2',
    });
    
    const client = new AWS.Rekognition();
    const params = {
      SourceImage: {
        Bytes: photo_source,
      },
      TargetImage: {
        Bytes: photo_target,
      },
      SimilarityThreshold: 70,
    }

    console.log('\nCOMPARING FACES............\n')
    var list = []
    client.compareFaces(params, this.dealWithResponse.bind(this));
  }

  dealWithResponse(err, response) {
    if (err) {
      console.log('\nERROR COMPARING FACES\n')
      console.log(err);
    } else {
      console.log("\n....RESULTS.....\n")
      response.FaceMatches.forEach(data => {
        let position   = data.Face.BoundingBox;
        let result = data.Similarity;
        this.setState({ similarities: result });
        console.log(`The face at: ${position.Left}, ${position.Top} matches with ${result} % confidence`);
        console.log(this.state.similarities);
      })
    }
  }

  
}